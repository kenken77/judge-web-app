'use strict';
const domain_name = process.env.DOMAIN;
module.exports = {
    domain_name: domain_name,
    mongodb: process.env.MONGODB_URL,
    port: process.env.PORT,
    session_secret: process.env.SECRET
}