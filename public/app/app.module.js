(function () {
    var myApp = angular
        .module("JudgeSheetApp", [
            'ui.router',
            'ui.bootstrap',
            'LocalStorageModule',
            'ngSanitize',
            'angular-growl',
            'oc.lazyLoad'
        ]);

    myApp.config(function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('JudgeSheetApp')
            .setStorageType('sessionStorage')
            .setNotify(true, true)
        });
})();